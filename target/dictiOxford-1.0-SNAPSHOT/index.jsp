<%-- 
    Document   : index
    Created on : 07-may-2020, 19:04:41
    Author     : javi3
--%>

<%@page import="root.entities.Palabra"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% Palabra dct = new Palabra();
%>
<html>
    <head>       
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dictionary</title>         
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    </head>
    <body>

        <header>
            <div class="navbar navbar-dark bg-dark shadow-sm">
                <div class="container d-flex justify-content-between">
                    <a href="#" class="navbar-brand d-flex align-items-center">
                        <h1>Dictionary</h1>
                    </a> 
                    <div> 
                        <form action="controller" method="POST">      
                            <input type="text" name="palabra" value="" placeholder="Ingresar palabra"/>      
                            <input type="submit" value="buscar" />  
                        </form>
                       </div>
                 </div>
            </div>      
           </header> 
        
                    <div style="padding-left: 20px;padding-right: 1000px;margin-top: 20px;margin-bottom: 20px;margin-left: 20px;margin-right: 20px;border-right-style: solid;border-right-width: 0px;">
                      <b>palabra</b> 
                      <p><input type="text" class="form-control" name="word" value="<%= dct.getIdword()%>"readonly/>
                    </div>
                        
                    <div class="p-3 mb-2 bg-dark text-white" style="right: 0px;padding-left: 400px;padding-right: 100px;padding-bottom: 16px; ">
                         
                        <ul type="circle"><b>Definicion</b>
                            <li placeholcer="Descripcion"> <%= dct.getDef()%></li>
                        </ul>
                    </div>
             
    </body>
</html>
