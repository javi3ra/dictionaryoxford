/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author javi3
 */
@Entity
@Table(name = "palabras")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Palabra.findAll", query = "SELECT p FROM Palabra p"),
    @NamedQuery(name = "Palabra.findByIdword", query = "SELECT p FROM Palabra p WHERE p.idword = :idword"),
    @NamedQuery(name = "Palabra.findByDef", query = "SELECT p FROM Palabra p WHERE p.def = :def")})
public class Palabra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "idword")
    private String idword;
    @Size(max = 2147483647)
    @Column(name = "def")
    private String def;

    public Palabra() {
    }

    public Palabra(String idword) {
        this.idword = idword;
    }

    public String getIdword() {
        return idword;
    }

    public void setIdword(String idword) {
        this.idword = idword;
    }

    public String getDef() {
        return def;
    }

    public void setDef(String def) {
        this.def = def;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idword != null ? idword.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Palabra)) {
            return false;
        }
        Palabra other = (Palabra) object;
        if ((this.idword == null && other.idword != null) || (this.idword != null && !this.idword.equals(other.idword))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entities.Palabra[ idword=" + idword + " ]";
    }
    
}
