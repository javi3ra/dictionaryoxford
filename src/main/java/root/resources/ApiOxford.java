/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.resources;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dataoxford.Dictionary;

/**
 *
 * @author javi3
 */
@Path("diccionario")
public class ApiOxford {
//    private final Logger log = Logger.getLogger(ApiOxford.class.getName());

    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(@HeaderParam("api-key") String apikey,
            @HeaderParam("api-id") String apiId,
            @PathParam("idbuscar") String buscar) {
        
        System.out.println("REST diccionario :    palabra  " + buscar);
        System.out.println("REST diccionario:  apiKEY " + apikey);
        System.out.println("REST diccionario:  apiID " + apiId);

        Dictionary dct = new Dictionary();
        dct.setDictionary(buscar);
        dct.setDescripcion("definicion");
        return Response.ok(200).entity(dct).build();

    }

}
