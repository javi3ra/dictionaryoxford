/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  https://od-api.oxforddictionaries.com/api/v2 esto es solo para guardar el link
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import root.entities.Palabra;
import root.entities.dao.PalabraDAO;


/**
 *
 * @author javi3
 */
@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
         
        root.dataoxford.Dictionary app= new root.dataoxford.Dictionary ();
        String palabra = request.getParameter("palabra");

        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("http://DESKTOP-T278FK5:8080/dictiOxford-1.0-SNAPSHOT/api/diccionario/" + palabra);
        app = myResource.request(MediaType.APPLICATION_JSON).header("api-key","4b120cfd0e759766cc1f71f45f43df02").header("api-id", "a4cb633b").get( root.dataoxford.Dictionary .class);
    // setear objeto a la database
       
       Palabra word=new Palabra();
       word.setIdword(app.getDictionary());
       word.setDef(app.getDescripcion());
       PalabraDAO dao=new PalabraDAO();
        try {
            dao.create(word);        
            request.getRequestDispatcher("index.jsp").forward(request,response);
        } catch (Exception ex) {
            System.out.print("ocurrio algo: "+ex);
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
           request.getRequestDispatcher("index.jsp").forward(request,response);

        }
             request.getRequestDispatcher("index.jsp").forward(request,response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
