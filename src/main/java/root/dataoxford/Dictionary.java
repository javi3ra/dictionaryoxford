/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dataoxford;

import java.io.Serializable;

/**
 *
 * @author javi3
 */
public class Dictionary implements Serializable {

    private String dictionary;
    private String descripcion;

    public Dictionary() {
    }

    public String getDictionary() {
        return dictionary;
    }

    public void setDictionary(String dictionary) {
        this.dictionary = dictionary;
    }
    public String getDescripcion() {
        return descripcion;
    }
     public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
